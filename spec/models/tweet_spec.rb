require 'rails_helper'

RSpec.describe Tweet, type: :model do
  describe '#validation' do
    let(:tweet) { create(:tweet) }

    it { should validate_length_of(:body).is_at_most(180) }
    it 'validates uniqueness recent body' do
      new_tweet = build(:tweet, user: tweet.user, body: tweet.body)

      new_tweet.valid?

      expect(new_tweet.errors).to include(:body)
      expect(new_tweet.errors[:body]).to include("translation missing: en.activerecord.errors.models.tweet.attributes.body.uniqueness")
    end
  end
end
