class HomeController < ApplicationController
  TWEETS_PER_PAGE = 50

  def index
    @tweets = Tweet.joins(user: :company).order("id DESC").limit(TWEETS_PER_PAGE).offset(offset)

    @tweets = @tweets.not_affiliated if params[:affiliated] == false
  end
end
