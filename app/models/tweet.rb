# == Schema Information
#
# Table name: tweets
#
#  id         :integer          not null, primary key
#  body       :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer
#
# Foreign Keys
#
#  user_id  (user_id => users.id)
#
class Tweet < ApplicationRecord
  belongs_to :user

  scope :not_affiliated, -> { joins(:user).where(user: { company: nil }) }

  validates :body, length: { maximum: 180 }
  validate :unique_recent_body

  private

  def unique_recent_body
    scope = Tweet.where(created_at: 24.hours.ago.., user:, body:)

    errors.add(:body, :uniqueness) if scope.present?
  end
end
